CREATE SCHEMA if not exists `commande` DEFAULT CHARACTER SET utf8 ;

INSERT INTO `commande`.`type_de_preparation` (`code`, `libelle`) VALUES ('boulangerie', 'Boulangerie');
INSERT INTO `commande`.`type_de_preparation` (`code`, `libelle`) VALUES ('patisserie', 'Patisserie');
INSERT INTO `commande`.`type_de_preparation` (`code`, `libelle`) VALUES ('autre', 'Autre');

(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('CommandeServiceModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('CommandeService', CommandeService);

	/**
	 * Injection des dépendances
	 */
	CommandeService.$inject = ['$rootScope', '$http', '$httpParamSerializer'];

	/**
	 * Implémentation du service
	 */
	function CommandeService($rootScope, $http, $httpParamSerializer) {

		var service = {
				getByDateCurrentMonth : getByDateCurrentMonth,
				getAllPageable : getAllPageable,
				count : count,
				exporter : exporter
		};

		return service;
		
		
		/**
		 * Récupère le objets en fonction de l'url correspondant à l'id.
		 * @returns Le objets en fonction de l'url correspondant à l'id.
		 */
		function getByDateCurrentMonth(date) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/commande/currentMonth/' + date,
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		function getAllPageable(page, offset, recherche) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/commande/pagine/' + page + '/' + offset + "?" + $httpParamSerializer(recherche),
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		function count(page) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/commande/count',
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		function exporter() {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/exporter',
			})
		}
		
	}
	
})();
(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('UserServiceModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('UserService', UserService);

	/**
	 * Injection des dépendances
	 */
	UserService.$inject = ['$rootScope', '$http'];

	/**
	 * Implémentation du service
	 */
	function UserService($rootScope, $http) {

		var service = {
				getConnectedUser : getConnectedUser
		};

		return service;
		
		
		function getConnectedUser() {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/user',
			})
		}
		
	}
	
})();
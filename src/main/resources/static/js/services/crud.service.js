(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('CrudServiceModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('CrudService', CrudService);

	/**
	 * Injection des dépendances
	 */
	CrudService.$inject = ['$rootScope', '$http'];

	/**
	 * Implémentation du service
	 */
	function CrudService($rootScope, $http) {

		var service = {
			getAll : getAll,
			get : get,
			update : update,
			create : create,
			supprime : supprime
		};

		return service;
		
		/**
		 * Récupère tous les objets en fonction de l'url
		 * @returns Tous les objets en fonction de l'url
		 */
		function getAll(urlService) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + urlService
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		/**
		 * Récupère le objets en fonction de l'url correspondant à l'id.
		 * @returns Le objets en fonction de l'url correspondant à l'id.
		 */
		function get(id, urlService) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + urlService + id,
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		/**
		 * Crée l'objet en fonction de l'url.
		 * @Returns L'objet en fonction de l'url créé.
		 */
		function create(objet, urlService) {
			
			return $http({
				method: 'POST',
				url: $rootScope.URL_BASE + urlService,
				data: objet
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		/**
		 * Met à jour l'objet en fonction de l'url.
		 * @Returns L'objet en fonction de l'url mis à jour.
		 */
		function update(objet, urlService) {
			
			return $http({
				method: 'PUT',
				url: $rootScope.URL_BASE + urlService,
				data: objet
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
		/**
		 * Met à jour l'objet en fonction de l'url.
		 * @Returns L'objet en fonction de l'url mis à jour.
		 */
		function supprime(id, urlService) {
			
			return $http({
				method: 'DELETE',
				url: $rootScope.URL_BASE + urlService + id,
			})
			.then(function() {
			})
		}
	}
	
})();
(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('ClientServiceModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('ClientService', ClientService);

	/**
	 * Injection des dépendances
	 */
	ClientService.$inject = ['$rootScope', '$http', '$httpParamSerializer'];

	/**
	 * Implémentation du service
	 */
	function ClientService($rootScope, $http, $httpParamSerializer) {

		var service = {
				getAllSearch : getAllSearch
		};

		return service;
		
		function getAllSearch(recherche) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/clients/recherche' + "?" + $httpParamSerializer(recherche),
			})
			.then(function(_data) {
				return _data.data;
			})
		}
	}
	
})();
(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('PreparationServiceModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('PreparationService', PreparationService);

	/**
	 * Injection des dépendances
	 */
	PreparationService.$inject = ['$rootScope', '$http'];

	/**
	 * Implémentation du service
	 */
	function PreparationService($rootScope, $http) {

		var service = {
			getByTypePreparation : getByTypePreparation,
		};

		return service;
		
		
		/**
		 * Récupère le objets en fonction de l'url correspondant à l'id.
		 * @returns Le objets en fonction de l'url correspondant à l'id.
		 */
		function getByTypePreparation(typePreparation) {
			
			return $http({
				method: 'GET',
				url: $rootScope.URL_BASE + '/preparation/getByType/' + typePreparation,
			})
			.then(function(_data) {
				return _data.data;
			})
		}
		
	}
	
})();
(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('RechercheRoutesModule', [
            // Modules injectés
		])
		.config(RechercheRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function RechercheRoutes($routeProvider) {
		$routeProvider
		.when('/recherche', {
			templateUrl: 'js/recherche/recherche.html',
			controller: 'RechercheController',
			controllerAs: 'vm'
		})
	}

})();
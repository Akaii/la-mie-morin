(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('RechercheControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('RechercheController', RechercheController);

	/**
	 * Injection des dépendances
	 */
	RechercheController.$inject = ['CommandeService', 'calendarConfig', 'ModalRechercheFactory', '$location'];

	/**
	 * Implémentation du controleur
	 */
	function RechercheController(CommandeService, calendarConfig, ModalRechercheFactory, $location) {
		var vm = this;
		
		vm.headingTitle = "Liste de commande";
		vm.events = [];
		
		// Récupération des commandes.
		CommandeService.getByDateCurrentMonth(moment().startOf('month').toDate())
		.then(function(commandes) {
			// Parcours des commandes.
			angular.forEach(commandes, function(commandeObjet, commandeKey) {
				var dateLivraison =  moment(commandeObjet.dateLivraison).format('MM/DD/YYYY');
				dateLivraison = moment(dateLivraison + " " + commandeObjet.heureLivraison).format('MM/DD/YYYY HH:mm:ss');
				dateLivraison = new Date(dateLivraison);
				if(commandeObjet.paye) {
					vm.color = calendarConfig.colorTypes.success;
				} else {
					vm.color = calendarConfig.colorTypes.important;
				}
				var nomPrenom = commandeObjet.client.nom + "  " + (commandeObjet.client.prenom === null ? "" : commandeObjet.client.prenom);
				vm.events.push({
					title: nomPrenom,
					color: vm.color,
					startsAt:dateLivraison,
					endsAt: moment(dateLivraison).add(2, 'hours').toDate(),
					draggable: true,
					resizable: true,
					actions: actions,
					commande: commandeObjet,
					quantites: commandeObjet.quantites,
				})
			});
		});
		
		
	    // Définition de la vue du calendrier par mois.
	    vm.calendarView = 'month';
	    vm.viewDate = new Date();
	    var actions = [{
	      label: '<i class=\'glyphicon glyphicon-pencil\'></i>',
	      onClick: function(args) {
	    	  ModalRechercheFactory.show('Edited', args.calendarEvent);
	      }
	    }, {
	      label: '<i class=\'glyphicon glyphicon-remove\'></i>',
	      onClick: function(args) {
	    	  ModalRechercheFactory.show('Deleted', args.calendarEvent);
	      }
	    }];
	
	
	    vm.addEvent = function() {
	      vm.events.push({
	        title: 'New event',
	        startsAt: moment().startOf('day').toDate(),
	        endsAt: moment().endOf('day').toDate(),
	        color: calendarConfig.colorTypes.important,
	        draggable: true,
	        resizable: true
	      });
	    };
	
	    vm.eventClicked = function(event) {
	    	ModalRechercheFactory.show('Clicked', event);
	    };
	
	    vm.eventEdited = function(event) {
	      ModalRechercheFactory.show('Edited', event);
	    };
	
	    vm.eventDeleted = function(event) {
	      ModalRechercheFactory.show('Deleted', event);
	    };
	
	    vm.eventTimesChanged = function(event) {
	      ModalRechercheFactory.show('Dropped or resized', event);
	    };
	
	    vm.toggle = function($event, field, event) {
	      $event.preventDefault();
	      $event.stopPropagation();
	      event[field] = !event[field];
	    };
	
	    vm.timespanClicked = function(date, cell) {
	
	      if (vm.calendarView === 'month') {
	        if ((vm.cellIsOpen && moment(date).startOf('day').isSame(moment(vm.viewDate).startOf('day'))) || cell.events.length === 0 || !cell.inMonth) {
	          vm.cellIsOpen = false;
	        } else {
	          vm.cellIsOpen = true;
	          vm.viewDate = date;
	        }
	      } else if (vm.calendarView === 'year') {
	        if ((vm.cellIsOpen && moment(date).startOf('month').isSame(moment(vm.viewDate).startOf('month'))) || cell.events.length === 0) {
	          vm.cellIsOpen = false;
	        } else {
	          vm.cellIsOpen = true;
	          vm.viewDate = date;
	        }
	      }
	
	    };
	}
	
})();
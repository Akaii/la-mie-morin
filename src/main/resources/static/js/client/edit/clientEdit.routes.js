(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('ClientEditRoutesModule', [
            // Modules injectés
		])
		.config(ClientEditRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function ClientEditRoutes($routeProvider) {
		$routeProvider
		.when('/client/creer', {
			templateUrl: 'js/client/edit/clientEdit.html',
			controller: 'ClientEditController',
			controllerAs: 'vm'
		}).when('/client/modifier' + '/:id', {
			templateUrl: 'js/client/edit/clientEdit.html',
			controller: 'ClientEditController',
			controllerAs: 'vm'
		});
	}

})();
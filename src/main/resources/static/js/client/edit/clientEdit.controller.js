(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('ClientEditControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('ClientEditController', ClientEditController);

	/**
	 * Injection des dépendances
	 */
	ClientEditController.$inject = ['$rootScope', '$scope', '$window', '$location', '$routeParams', '$timeout', 'CrudService', 'URL_CLIENTS', 'URL_COMMANDE_MODIFIER'];

	/**
	 * Implémentation du controleur
	 */
	function ClientEditController($rootScope, $scope, $window, $location, $routeParams, $timeout, CrudService, URL_CLIENTS, URL_COMMANDE_MODIFIER) {
		var vm = this;

		vm.bouton = [];
		vm.commande = [];

		vm.lien = URL_COMMANDE_MODIFIER;
		
		vm.sortType = 'name'; // set the default sort type
		vm.sortReverse = false;  // set the default sort order
		vm.searchClient = '';
		
		vm.alerte = {};
		
		// On vérifie l'url afin de savoir si la page demandée est CREATION.
		vm.type = $location.url();
		
		vm.bouton.retour = "Retour";
		
		// Creation.
		if(vm.type.indexOf('creer') !== -1){
	
			vm.bouton.creerOuModifier = "Créer";
			
			vm.headingTitle = "Création d'un client";
		
			// Création d'un nouveau client. 
			vm.creerOuModifierClient = function (formulaireValide) {
				if(!formulaireValide)
					 return;
				CrudService.create(vm.client, '/client/').then(function(data) {
					$location.path(URL_CLIENTS);
			 	}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
				});
		     }
			
		} else 
		// Modification.
		if(vm.type.indexOf('modifier') !== -1){
			CrudService.get($routeParams.id,'/client/').then(function(client) {
				 vm.client = client;
				 vm.commande = client.commande;
			 });
			 vm.headingTitle = "Modification d'un client";
			 vm.bouton.creerOuModifier = "Modifier";
			 vm.bouton.supprimer = "Supprimer";
		
			 // Création d'un nouveau client.
			 vm.creerOuModifierClient = function (formulaireValide) {
				 if(!formulaireValide)
					 return;
			 	CrudService.update(vm.client, '/client/').then(function(client) {
			 		vm.alerte.type = 'succes';
			 		vm.alerte.texte = 'Le client a bien été modifié.';
			 		$timeout(function () { vm.alerte.type = undefined }, 3000);   
			 	}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
					$timeout(function () { vm.alerte.type = undefined }, 3000);   
				});
		      }
			 
			 
			 // Suppression d'un nouveau client. 
			 vm.supprime = function () {
				 CrudService.supprime($routeParams.id,'/client/').then(function() {
					 // Redirection vers la page d'acceuil.
					 $location.path(URL_CLIENTS);
			 	}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
				});
			 }
			 
			 vm.redirection = function (path) {
				 $location.path(path);
			 }
		}
		
		vm.back = function() {
			$window.history.back();
    	}
	}

})();


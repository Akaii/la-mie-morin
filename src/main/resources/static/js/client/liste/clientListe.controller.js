(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('ClientListeControllerModule', [
	        // Modules injectés
	]).controller('ClientListeController', ClientListeController);

	/**
	 * Injection des dépendances
	 */
	ClientListeController.$inject = ['$rootScope', '$scope', '$location', 'ClientService'];

	/**
	 * Implémentation du controleur
	 */
	function ClientListeController($rootScope, $scope, $location, ClientService) {
		var vm = this;
	
		vm.bouton = [];
		
		vm.lien = '/client/modifier';
		
		vm.recherche = {};
		
		/** Initialisation de la listeView collapsible. */
		vm.listView = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
		vm.clientListe = [];

		angular.forEach(vm.listView, function(listView) {
			vm.clientListe.push({titre : listView, client : []})
		});
		
		vm.headingTitle = "Liste de clients";
		vm.bouton.lien = '/client/creer';
		vm.bouton.nom = "Créer un client";
		ClientService.getAllSearch(vm.recherche).then(function(clients) {
	    	angular.forEach(clients, function(client) {
    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
	    	}) 
	    });
		
		vm.rechercher = function () {
			ClientService.getAllSearch(vm.recherche).then(function(clients) {
				vm.clientListe = [];

				angular.forEach(vm.listView, function(listView) {
					vm.clientListe.push({titre : listView, client : []})
				});
		    	angular.forEach(clients, function(client) {
	    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
		    	}) 
		    });
		}
		
		vm.effacerRecherche = function() {
	    	vm.recherche = {};
			ClientService.getAllSearch(vm.recherche).then(function(clients) {
		    	angular.forEach(clients, function(client) {
	    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
		    	}) 
		    });
	    }
	    
	    vm.redirection = function (path) {
	    	$location.path(path);
		};
	    
	}

})();
(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('ClientListeRoutesModule', [
            // Modules injectés
		])
		.config(ClientListeRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function ClientListeRoutes($routeProvider) {
		$routeProvider
		.when('/client/liste', {
			templateUrl: 'js/client/liste/clientListe.html',
			controller: 'ClientListeController',
			controllerAs: 'vm'
		})
	}

})();
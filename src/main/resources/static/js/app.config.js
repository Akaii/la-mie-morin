(function() {
	'use strict';
	/**
	 * Déclaration de la configuration de l'application
	 */
	angular.module('boulangerieApp')
		.config(function($routeProvider, $httpProvider) {
			$httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
			$httpProvider.interceptors.push(function($q, $window, $rootScope) {
				return {
					'request' : function(config) {
						return config;
				    },
				    'requestError' : function(rejection) {
				    	return $q.reject(rejection);
				    },
				    'response' : function(response) {
				    	return response;
				    },
				    'responseError' : function(rejection) {
				    	// Interception des retours d'erreur du serveur (Tous les codes erreurs).
				    	var error = {texte: '', type: ''};
				    	
				    	// Traitement spécifique en fonction du HTTP Message.
						if (401 === rejection.status) {
						    $window.location = $rootScope.URL_BASE + '/login';
						} else if (500 === rejection.status) {
							error.type = "erreur";
							error.texte = "Une erreur technique est survenue, veuillez rééssayer.";
							return $q.reject(error);
						} else if (400 === rejection.status) {
							error.type = "erreur";
							error.texte = "La requête envoyé au serveur est incorrecte, veuillez contacter l'administrateur.";
							return $q.reject(error);
						} else if (405 === rejection.status) {
							error.type = "erreur";
							error.texte = "Cette fonctionnalité n'est pas disponible, veuillez contacter l'administrateur.";
							return $q.reject(error);
						}
						return $q.reject(rejection);
					}
				};
			});
		
		})
		.run(['$rootScope', '$location', '$window', 
		      function($rootScope, $location, $window) {
			
			$rootScope.user = {nom : '', role : ''};
			
			/**
			 * VARIABLES GLOBALES
			 */
			var pathArray = $window.location.pathname.split( '/' );
			$rootScope.URL_BASE = '/' + pathArray[1];
			
		}])
		.filter('capitalize', function() {
			return function(input) {
		    	return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
		    }
		});
})();
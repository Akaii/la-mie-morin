//// Directive permettant de ne pas perdre le style CSS des librairies JQ Mobile.
//app.directive('applyJqMobile', function() {
//	return function($scope, el) {
//		setTimeout(function() {
//			$scope.$on('$viewContentLoaded', el.trigger("create"))
//		}, 1);
//	}
//});
//
//app.directive('jqTable', function() {
//	return function($scope, el) {
//		$(el).hide();
//		setTimeout(function() {
//			$scope.$on('$viewContentLoaded', el.parent().listview('refresh'))
//		}, 1);
//		$(el).show();
//	}
//});
//
//// Permet de gérer le clic des boutons. (CF Client => Créer client)
//app.directive('goClick', function($location) {
//	return function(scope, element, attrs) {
//		var path;
//
//		attrs.$observe('goClick', function(val) {
//			path = val;
//		});
//
//		element.bind('click', function() {
//			scope.$apply(function() {
//				$location.path(path);
//			});
//		});
//	};
//});

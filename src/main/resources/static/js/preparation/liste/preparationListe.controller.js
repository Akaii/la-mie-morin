(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('PreparationListeControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('PreparationListeController', PreparationListeController);

	/**
	 * Injection des dépendances
	 */
	PreparationListeController.$inject = ['$location', 'PreparationService', 'URL_PREPARATION_CREER', 'URL_PREPARATION_MODIFIER'];

	/**
	 * Implémentation du controleur
	 */
	function PreparationListeController($location, PreparationService, URL_PREPARATION_CREER, URL_PREPARATION_MODIFIER) {
		var vm = this;

		vm.bouton = [];
		
		vm.lien = URL_PREPARATION_MODIFIER;
		
		vm.headingTitle = "Liste de préparation";
		vm.bouton.lien = URL_PREPARATION_CREER;
		vm.bouton.nom = "Créer une préparation";
		
		/** Initialisation de la listeView collapsible. */
		vm.listView = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9'];
		vm.preparationListeBoulangerie = [];
		vm.preparationListePatisserie = [];
		vm.preparationListeAutre = [];

		angular.forEach(vm.listView, function(listView) {
			vm.preparationListeBoulangerie.push({titre : listView, preparation : []});
			vm.preparationListePatisserie.push({titre : listView, preparation : []});
			vm.preparationListeAutre.push({titre : listView, preparation : []});
		});
		
		PreparationService.getByTypePreparation('Boulangerie').then(function(preparations) {
			// TODO
			angular.forEach(preparations, function(preparation) {
    			vm.preparationListeBoulangerie[vm.listView.indexOf(preparation.nomPreparation.toUpperCase().charAt(0))].preparation.push(preparation);
	    	}) 
			vm.preparationsBoulangerie = preparations;
	    });
		
		PreparationService.getByTypePreparation('Patisserie').then(function(preparations) {
			// TODO
			angular.forEach(preparations, function(preparation) {
    			vm.preparationListePatisserie[vm.listView.indexOf(preparation.nomPreparation.toUpperCase().charAt(0))].preparation.push(preparation);
	    	}) 
			vm.preparationsPatisserie = preparations;
	    });
		
		PreparationService.getByTypePreparation('Autre').then(function(preparations) {
			// TODO
			angular.forEach(preparations, function(preparation) {
    			vm.preparationListeAutre[vm.listView.indexOf(preparation.nomPreparation.toUpperCase().charAt(0))].preparation.push(preparation);
	    	}) 
			vm.preparationsAutres = preparations;
	    });
	    
	    vm.redirection = function (path) {
	    	$location.path(path);
		};
	}
})();
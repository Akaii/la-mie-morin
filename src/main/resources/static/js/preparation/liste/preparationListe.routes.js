(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('PreparationListeRoutesModule', [
            // Modules injectés
		])
		.config(PreparationListeRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function PreparationListeRoutes($routeProvider) {
		$routeProvider
		.when('/preparation/liste', {
			templateUrl: 'js/preparation/liste/preparationListe.html',
			controller: 'PreparationListeController',
			controllerAs: 'vm'
		})
	}

})();
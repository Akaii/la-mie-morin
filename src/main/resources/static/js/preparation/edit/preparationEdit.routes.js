(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('PreparationEditRoutesModule', [
            // Modules injectés
		])
		.config(PreparationEditRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function PreparationEditRoutes($routeProvider) {
		$routeProvider
		.when('/preparation/creer', {
			templateUrl: 'js/preparation/edit/preparationEdit.html',
			controller: 'PreparationEditController',
			controllerAs: 'vm'
		})
		.when('/preparation/modifier' + '/:id', {
			templateUrl: 'js/preparation/edit/preparationEdit.html',
			controller: 'PreparationEditController',
			controllerAs: 'vm'
		})
	}

})();
(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('PreparationEditControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('PreparationEditController', PreparationEditController);

	/**
	 * Injection des dépendances
	 */
	PreparationEditController.$inject = ['$location', '$routeParams', '$window', 'CrudService', 'CommandeService', 'URL_PREPARATION'];

	/**
	 * Implémentation du controleur
	 */
	function PreparationEditController($location, $routeParams, $window, CrudService, CommandeService, URL_PREPARATION) {
		var vm = this;
		
		vm.alerte = {};
		
		vm.bouton = [];
		vm.preparation = {};
		
		vm.bouton.retour = "Retour";
		
		// On vérifie l'url afin de savoir si la page demandée est CREATION.
		var type = $location.url();
		
		// Creation.
		if(type.indexOf('creer') !== -1){
			// Récupération des types de préparation.
			
			CrudService.getAll('/typeDePreparation').then(function(typePreparation) {
				vm.typePreparation = typePreparation;
			});
			
			vm.bouton.creerOuModifier = "Créer";
			
			vm.headingTitle = "Création d'une préparation";
		
			// Création d'une nouvelle préparation. 
			vm.creerOuModifierPreparation = function () {
				
				CrudService.create(vm.preparation, '/preparation/').then(function(preparation){
					// Redirection vers la page d'acceuil.
					$location.path(URL_PREPARATION);
					
					vm.alerte.type = 'succes';
					vm.alerte.texte = 'La préparation a bien été créée.';
				}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
				});
		     }
			
		} else 
		// Modification.
		if(type.indexOf('modifier') !== -1){
			
			// Récupération des types de préparation.
			
			 
			CrudService.get($routeParams.id, '/preparation/').then(function(typePreparation) {
				vm.preparation = typePreparation;
			});
			 
			CrudService.getAll('/typeDePreparation').then(function(typePreparation) {
				vm.typePreparation = typePreparation;
			});
			 
			vm.headingTitle = "Modification d'une préparation";
			vm.bouton.creerOuModifier = "Modifier";
			vm.bouton.supprimer = "Supprimer";
			
			// Création d'une préparation. 
			vm.creerOuModifierPreparation = function () {
				 
				CrudService.update(vm.preparation, '/preparation/').then(function(preparation){
					vm.alerte.type = 'succes';
					vm.alerte.texte = 'La préparation a bien été mise à jour.';
				}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
				});
					 
			}
			
			vm.supprimer = function () {
				CrudService.supprime($routeParams.id, '/preparation/').then(function(preparation){
					// Redirection vers la page d'acceuil.
					$location.path(URL_PREPARATION);
				}, function(error) {
					vm.alerte.type = error.type;
					vm.alerte.texte = error.texte;
				});
			}
			
			vm.back = function() {
				$window.history.back();
	    	}
		}
	}
})();
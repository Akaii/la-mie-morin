(function () {
	'use strict';

	/**
	 * Déclaration du module de l'application
	 */
	angular
		.module('boulangerieApp', [
		          'ui.bootstrap',
		          'ui.select',
				  'ngResource',
				  'ngRoute',
				  'mwl.calendar',
				  'ngAnimate',
				  
				  // Constantes
				  'constantes',
				  
				  // Routes
				  'ClientListeRoutesModule',
				  'ClientEditRoutesModule',
				  'CommandeListeRoutesModule',
				  'CommandeEditRoutesModule',
				  'PreparationListeRoutesModule',
				  'PreparationEditRoutesModule',
				  'RechercheRoutesModule',
				  'ModalRechercheFactoryModule',
				  'LoginRoutesModule',
				  
				  // Controller
				  'ClientListeControllerModule',
				  'ClientEditControllerModule',
				  'CommandeListeControllerModule',
				  'CommandeEditControllerModule',
				  'PreparationListeControllerModule',
				  'PreparationEditControllerModule',
				  'RechercheControllerModule',
				  'LoginControllerModule',
				  
				  // Services
				  'CrudServiceModule',
				  'PreparationServiceModule',
				  'CommandeServiceModule',
				  'UserServiceModule',
				  'ClientServiceModule',
				  
				  // Factory (Modal)
				  'ModalFactoryModule',
				  'ModalChoixPreparationFactoryModule',
				  
				  // Directives
				  'HeaderDirectiveModule',
				  ]);
})(); 
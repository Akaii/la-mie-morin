(function () {
	'use strict';

	/**
	 * Déclaration du module de l'application
	 */
	angular
		.module('constantes', [])
		// Clients.
		.constant('URL_CLIENTS', '/client/liste') 
		.constant('URL_CLIENTS_CREER', '/client/creer')
		.constant('URL_CLIENTS_MODIFIER', '/client/modifier')
		
		// Commandes.
		.constant('URL_COMMANDE', '/commande/liste')
		.constant('URL_COMMANDE_CREER', '/commande/creer')
		.constant('URL_COMMANDE_MODIFIER', '/commande/modifier')
		
		// Préparations.
		.constant('URL_PREPARATION', '/preparation/liste')
		.constant('URL_PREPARATION_CREER', '/preparation/creer')
		.constant('URL_PREPARATION_MODIFIER', '/preparation/modifier')
		
		// Recherche
		.constant('URL_RECHERCHE', '/admin/parametre/liste')
		
		// Modal (Pop-up).
		.constant('URL_MODAL', '/admin/parametre/liste')
		.constant('URL_CLIENTS_CREER', '/admin/parametre/liste')
		;
	
})(); 
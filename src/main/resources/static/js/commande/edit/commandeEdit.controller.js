(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('CommandeEditControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('CommandeEditController', CommandeEditController);

	/**
	 * Injection des dépendances
	 */
	CommandeEditController.$inject = ['$rootScope', '$scope', '$window', '$location', '$routeParams', '$timeout', 'CrudService', 'PreparationService', 'URL_COMMANDE', 'ModalFactory', 'ModalChoixPreparationFactory'];

	/**
	 * Implémentation du controleur
	 */
	function CommandeEditController($rootScope, $scope, $window, $location, $routeParams, $timeout, CrudService, PreparationService, URL_COMMANDE, ModalFactory, ModalChoixPreparationFactory) {
		var vm = this;
		
		vm.alerte = {};
		vm.index = 0;
		
		vm.bouton = [];
		vm.commande = {};
		vm.preparation = {};
		vm.quantite = {};
		
		vm.table = [];
		
		vm.infoBulle = "Coché si la commande est payée";
		vm.bouton.retour = "Retour";
		
		// On vérifie l'url afin de savoir si la page demandée est CREATION.
		var type = $location.url();
		
		// Permet de mettre à jour le prix en fonction de la quantité sélectionnée et de la préparation sélectionnée.
		vm.mettreAjourPrix = function() {
			var prix = vm.preparation.prix;
			// Faire opération.
			vm.quantite.prixCalcule = (prix * vm.quantite.quantite);
			
		}
		
		// Creation.
		if(type.indexOf('creer') !== -1){
			vm.commande = {dateCommande: new Date()};
			vm.bouton.creerOuModifier = "Créer";
			vm.bouton.ajouterClient = "Ajouter un client";
		} else if(type.indexOf('modifier') !== -1) {
			vm.bouton.creerOuModifier = "Modifier";
			vm.bouton.ajouterClient = "Modifier le client";
			CrudService.get($routeParams.id,'/commande/').then(function(commande) {
				vm.commande = commande;
				if(vm.commande.quantites != undefined) {
					vm.commande.prix = 0;
					angular.forEach(vm.commande.quantites, function(quantiteObjet, quantiteCle) {
						var row = {
								typePreparation : quantiteObjet.preparation.typePreparation,
								preparation : quantiteObjet.preparation,
								quantite : quantiteObjet.quantite,
								prixCalcule : quantiteObjet.preparation.prix  * quantiteObjet.quantite,
							};
						vm.commande.prix += row.prixCalcule;
						vm.table.push(row);
					});
					vm.commande.prix = vm.commande.prix.toFixed(2);
				}
				vm.commande.dateCommande = moment(vm.commande.dateCommande).toDate();
				vm.commande.dateLivraison = moment(vm.commande.dateLivraison).toDate();
				vm.commande.heureLivraison = moment(moment(new Date(), 'MM-DD-YYYY') + ' ' + vm.commande.heureLivraison, 'MM-DD-YYYY HH:mm:SS').toDate();
			}, function(error) {
				vm.alerte.type = error.type;
				vm.alerte.texte = error.texte;
			});
		}
			CrudService.getAll('/clients/').then(function(clients) {
				vm.clients = client;
			});
			
			vm.modal = function() {
				ModalFactory.show('ajouterClient').result.then(function() {
					vm.commande.client = ModalFactory.getClient();
				});
		    };
			
			vm.bouton.ajouterLignes = "Ajouter à la commande";
			vm.titreModal = "Sélection d'un client pour la commande";
			vm.headingTitle = "Création d'une commande";
			
			// Création d'une nouvelle commande. 
			vm.creerOuModifierCommande = function () {
				vm.commande.heureLivraison = moment(vm.commande.heureLivraison).format('HH:mm:SS');
				if(vm.bouton.creerOuModifier.indexOf('Modifier') !== -1) {
					CrudService.update(vm.commande, '/commande/').then(function(commande){
						// Redirection vers la page d'acceuil.
//						$location.path(URL_COMMANDE);
						vm.alerte.type = 'succes';
						vm.alerte.texte = 'La commande a bien été modifiée.';
						$window.scrollTo(0, 0);
				 		$timeout(function () { vm.alerte.type = undefined }, 3000);   
					}, function(error) {
						vm.alerte.type = error.type;
						vm.alerte.texte = error.texte;
				 		$timeout(function () { vm.alerte.type = undefined }, 3000);  
					});
				} else {
					vm.commande.dateCommande = moment(vm.commande.dateCommande).toDate();
					vm.commande.dateLivraison = moment(vm.commande.dateLivraison).utc(1).toDate();
					CrudService.create(vm.commande, '/commande/').then(function(commande){
						// Redirection vers la page d'acceuil.
						$location.path(URL_COMMANDE);
					}, function(error) {
						vm.alerte.type = error.type;
						vm.alerte.texte = error.texte;
						$timeout(function () { vm.alerte.type = undefined }, 3000);  
					});
				}
			}		
			// Méthode pour enlever des préparation de la commande.
			vm.enleverLigne = function(row) {
				// Récupération de l'index de la préparation à enlever de l'entité quantité.
				var index = vm.table.indexOf(row);
				// Mise à jour du prix.
				vm.commande.prix = (vm.commande.prix - row.prixCalcule).toFixed(2);
				// Récupération de l'index de la ligne à enlever.
				vm.table.splice(index,1);
				// Récupération de la préparation à enlever.
				vm.quantite.preparation = row.preparation;
				vm.quantite.prixCalcule = row.prixCalcule;
				vm.commande.quantites.splice(index, 1);
			}
			
			// Ajout d'un client dans le scope commande.
			vm.ajouterClient = function (client) {
				vm.commande.client = client;
				$( "#myPanel" ).panel( "close" );
			}
			
			// Ouverture de la modal des préparations.
			vm.modalPreparation = function() {
				var prix;
				
				ModalChoixPreparationFactory.show().result.then(function() {
					vm.quantite = ModalChoixPreparationFactory.getQuantite();
					vm.table.push(ModalChoixPreparationFactory.getRow());
					
					if(vm.commande.prix === undefined) {
						prix = Number(0);
					} else {
						prix = Number(vm.commande.prix);
					}
					
					// Récupération des champs déjà settés.
					var numCommande = vm.commande.numCommande;
					var dateCommande = vm.commande.dateCommande;
					var numeroCommande = vm.commande.numeroCommande;
					var dateLivraison = vm.commande.dateLivraison;
					var heureLivraison = vm.commande.heureLivraison;
					var paye = vm.commande.paye;
					var receptionne = vm.commande.receptionne;
					var quantites = vm.commande.quantites !== undefined ? vm.commande.quantites : [];
					var client = vm.commande.client;
					
					vm.commande = ModalChoixPreparationFactory.getCommande();
					vm.commande.dateCommande = dateCommande;
					vm.commande.dateLivraison = dateLivraison;
					vm.commande.heureLivraison = heureLivraison;
					vm.commande.client = client;
					vm.commande.paye = paye;
					vm.commande.receptionne = receptionne;
					vm.commande.numeroCommande = numeroCommande;
					vm.commande.numCommande = numCommande;
					quantites.push(vm.commande.quantites[0]);
					vm.commande.quantites = quantites;
					vm.commande.prix = (Number(prix) + Number(vm.quantite.prixCalcule)).toFixed(2);
				});
		    };
		    
		    vm.back = function() {
				$window.history.back();
	    	}
	}

})();
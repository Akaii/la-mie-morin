(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('CommandeEditRoutesModule', [
            // Modules injectés
		])
		.config(CommandeEditRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function CommandeEditRoutes($routeProvider) {
		$routeProvider
		.when('/commande/creer', {
			templateUrl: 'js/commande/edit/commandeEdit.html',
			controller: 'CommandeEditController',
			controllerAs: 'vm'
		}).when('/commande/modifier' + '/:id', {
			templateUrl: 'js/commande/edit/commandeEdit.html',
			controller: 'CommandeEditController',
			controllerAs: 'vm'
		});
	}

})();
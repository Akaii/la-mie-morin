(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('CommandeListeRoutesModule', [
            // Modules injectés
		])
		.config(CommandeListeRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function CommandeListeRoutes($routeProvider) {
		$routeProvider
		.when('/commande/liste', {
			templateUrl: 'js/commande/liste/commandeListe.html',
			controller: 'CommandeListeController',
			controllerAs: 'vm'
		})
	}

})();
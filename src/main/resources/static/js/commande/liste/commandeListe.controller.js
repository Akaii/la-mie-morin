(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('CommandeListeControllerModule', [
	        // Modules injectés
	        'constantes'
	]).controller('CommandeListeController', CommandeListeController);

	/**
	 * Injection des dépendances
	 */
	CommandeListeController.$inject = ['$scope', '$route', '$window', '$location', '$httpParamSerializer', 'CrudService', 'CommandeService', 'URL_COMMANDE_CREER', 'URL_COMMANDE_MODIFIER'];

	/**
	 * Implémentation du controleur
	 */
	function CommandeListeController($scope, $route, $window, $location, $httpParamSerializer, CrudService, CommandeService, URL_COMMANDE_CREER, URL_COMMANDE_MODIFIER) {
		var vm = this;
		
		vm.alerte = {};
		
		vm.recherche = {
						sortType:"dateLivraison",
						sortReverse:true
						};
		
		vm.commandesParPage = 10;
		
		vm.objetsParPage = [
		                    {
		                    	index : 0,
		                    	offset : 10,
		                    	select : true
		                    },
		                    {
		                    	index : 1,
		                    	offset : 25,
		                    	select : false
		                    },
		                    {
		                    	index : 2,
		                    	offset : 50,
		                    	select : false
		                    }];
		
		vm.headingTitle = "Liste de commande";
		vm.titreRecherche = "Rechercher une commande";
		vm.pageCourante = 1;
		vm.pageMax = 1;
		vm.pages = [];
		vm.bouton = [];
		
		vm.lien = URL_COMMANDE_MODIFIER;
		
		vm.bouton.lien = URL_COMMANDE_CREER;
		vm.bouton.nom = "Créer une commande";
	    
		var constructionWidgetPagination = function() {
			vm.pages = [];
			CommandeService.count().then(function(nombreCommande){
				var isActif = true;
				vm.pageMax = Math.ceil(nombreCommande / vm.commandesParPage);
				for(var i = 1; i <= vm.pageMax; i++) {
					if(i > 1) {
						isActif = false;
					}
					vm.pages.push({
						numero : i,
						actif : isActif
					});
				}
			});
			
		}
		
		if(vm.commandes === undefined) {
			CommandeService.getAllPageable(0, vm.commandesParPage, vm.recherche).then(function(commandes){
				vm.commandes = commandes;
				affichageDateEtNombreCommande(commandes);
				if(commandes.length > 0) 
					constructionWidgetPagination();
			});
		}
		
		$scope.$watch('vm.recherche._dateLivraison', function() {
			if(vm.recherche._dateLivraison)
				vm.recherche.dateLivraison = moment(vm.recherche._dateLivraison).utc(1).toDate();
		});
		
		vm.rechercher = function () {
			vm.pageCourante = 1;
			vm.pageMax = 1;
			vm.pages = [];
			CommandeService.getAllPageable(0, vm.commandesParPage, vm.recherche).then(function(commandes){
	    		vm.commandes = commandes;
	    		affichageDateEtNombreCommande(commandes);
	    		if(commandes.length > 0) 
					constructionWidgetPagination();
	    	});
		}
		
	    vm.selectPage = function(page, disabled) {
	    	if(disabled) return;
	    	CommandeService.getAllPageable(page - 1, vm.commandesParPage, vm.recherche).then(function(commandes){
	    		vm.pageCourante = page;
	    		vm.commandes = commandes;
	    		affichageDateEtNombreCommande(commandes);
	    		angular.forEach(vm.pages, function(p, index) {
	    			if(p.numero === page) {
	    				p.actif = true;
	    			} else {
	    				p.actif = false;
	    			}
	    		});
	    	});
	    }
	    
	    vm.effacerRecherche = function() {
	    	vm.recherche = {
					sortType:"dateLivraison",
					sortReverse:true
					};
	    	vm.pageCourante = 1;
			vm.pageMax = 1;
			vm.pages = [];
	    	CommandeService.getAllPageable(0, vm.commandesParPage, vm.recherche).then(function(commandes){
	    		vm.commandes = commandes;
	    		affichageDateEtNombreCommande(commandes);
	    		if(commandes.length > 0) 
					constructionWidgetPagination();
	    	});
	    }
	    
	    var affichageDateEtNombreCommande = function(commandeObj) {
	    	angular.forEach(vm.commandes, function(commandeObjet, commandeKey) {
	    		angular.forEach(commandeObjet, function(obj, key) {
					if(key === 'dateCommande') {
						vm.commandes[commandeKey][key] = moment(obj).format('DD/MM/YYYY');
					} else if (key === 'dateLivraison') {
						var date = moment(obj);
						var heure = moment(vm.commandes[commandeKey].heureLivraison, 'HH::mm:ss');
						date.hour(heure.get('hour'));
						date.minute(heure.get('minute'));
						date.second(heure.get('second'));
						vm.commandes[commandeKey][key] = moment(date).utcOffset(-10, true).format('DD/MM/YYYY HH:mm:ss');
					} else if (key === 'numCommande') {
						if(vm.commandes[commandeKey][key] !== null && !isNaN(parseInt(obj)))
							vm.commandes[commandeKey][key] = parseInt(obj);
					}
	    		});
	    	});
	    };
	    
	    vm.redirection = function (path) {
	    	$location.path(path);
	    }
	    
		 // Suppression d'une commande. 
		 vm.supprimer = function (numeroCommande) {
			 CrudService.supprime(numeroCommande, '/commande/').then(function() {
				 $route.reload();
			 },function(error){
				 $window.alert('Une erreur est survenue');
			 });
		 }
		 
		 vm.choixDonneesParPage = function (obj, selected) {
			 if(selected) return;
			 angular.forEach(vm.objetsParPage, function(objet, id) {
				 if(obj.index === id) {
					 vm.commandesParPage = objet.offset = obj.offset;
					 objet.select = true;
				 } else {
					 objet.select = false;
				 }
			 });
			 vm.pageCourante = 1;
				vm.pageMax = 1;
				vm.pages = [];
			 CommandeService.getAllPageable(0, vm.commandesParPage, vm.recherche).then(function(commandes){
					vm.commandes = commandes;
					affichageDateEtNombreCommande(commandes);
					if(commandes.length > 0) 
						constructionWidgetPagination();
				});
		 }
		 
		 vm.exporterCommandes = function() {
			 $window.location = 'exporter' + "?" + $httpParamSerializer(vm.recherche);
		 }
		 
		 vm.exporterPreparation = function() {
			 $window.location = 'preparation/exporter' + "?" + $httpParamSerializer(vm.recherche);
		 }
	}

})();
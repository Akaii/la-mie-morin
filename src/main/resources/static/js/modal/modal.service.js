(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('ModalFactoryModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('ModalFactory', ModalFactory);

	/**
	 * Injection des dépendances
	 */
	ModalFactory.$inject = ['$rootScope', '$http', '$uibModal', 'ClientService'];

	/**
	 * Implémentation du service
	 */
	function ModalFactory($rootScope, $http, $uibModal, ClientService) {

		var client = {};
		
		function show() {
			return $uibModal.open({
				templateUrl: 'js/modal/modalClient.html',
				controller: function(ModalFactory) {
					var vm = this;
					
					vm.bouton = [];
					
					vm.lien = '/client/modifier';
					
					/** Initialisation de la listeView collapsible. */
					vm.listView = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
					vm.clientListe = [];

					angular.forEach(vm.listView, function(listView) {
						vm.clientListe.push({titre : listView, client : []})
					});
					
					vm.headingTitle = "Liste de clients";
					vm.bouton.lien = '/client/creer';
					vm.bouton.nom = "Créer un client";
					ClientService.getAllSearch(vm.recherche).then(function(clients) {
				    	angular.forEach(clients, function(client) {
			    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
				    	}) 
				    });
				    
				    vm.rechercher = function () {
						ClientService.getAllSearch(vm.recherche).then(function(clients) {
							vm.clientListe = [];

							angular.forEach(vm.listView, function(listView) {
								vm.clientListe.push({titre : listView, client : []})
							});
					    	angular.forEach(clients, function(client) {
				    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
					    	}) 
					    });
					}
					
					vm.effacerRecherche = function() {
				    	vm.recherche = {};
						ClientService.getAllSearch(vm.recherche).then(function(clients) {
					    	angular.forEach(clients, function(client) {
				    			vm.clientListe[vm.listView.indexOf(client.nom.toUpperCase().charAt(0))].client.push(client);
					    	}) 
					    });
				    }
				    
					vm.selectionClient = function (clientSelectionne) {
						vm.client = ModalFactory.client = clientSelectionne;
						if(clientSelectionne === undefined)
							return;
						$('#modalwindow').modal('hide');
					};
					
				},
				controllerAs: 'vm',
				resolve : {
					client : function() {
						if(client) {
							return client;
						}
					}
				}
			});
		}
		
		function getClient() {
			return this.client;
		}
		
		return {
			show: show,
			client : client,
			getClient : getClient
		};
		
	}
	
})();
(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('ModalRechercheFactoryModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('ModalRechercheFactory', ModalRechercheFactory);

	/**
	 * Injection des dépendances
	 */
	ModalRechercheFactory.$inject = ['$rootScope', '$http', '$uibModal', 'CrudService'];

	/**
	 * Implémentation du service
	 */
	function ModalRechercheFactory($rootScope, $http, $uibModal, CrudService) {

		var client = {};
		
		function show(action, event) {
			return $uibModal.open({
				templateUrl: 'js/modal/recherche/modalContent.html',
				controller: function() {
					var vm = this;
					vm.action = action;
					vm.event = event;
				},
				controllerAs: 'vm'
			});
		}
		return {
			show: show
		};
		
	}
	
})();
(function() {
	'use strict';

	/**
	 * Déclaration du module
	 */
	angular
		.module('ModalChoixPreparationFactoryModule', [
		    // Modules injectés
		])
		// Déclaration du service
		.factory('ModalChoixPreparationFactory', ModalChoixPreparationFactory);

	/**
	 * Injection des dépendances
	 */
	ModalChoixPreparationFactory.$inject = ['$rootScope', '$http', '$uibModal', 'CrudService', 'PreparationService'];

	/**
	 * Implémentation du service
	 */
	function ModalChoixPreparationFactory($rootScope, $http, $uibModal, CrudService, PreparationService) {

		var quantite = {};
		var commande = {};
		var row = {};

		function show() {
			return $uibModal.open({
				templateUrl: 'js/modal/preparation/modalChoixPreparation.html',
				size : 'lg',
				controller: function(ModalChoixPreparationFactory) {
					var vm = this;
					
					vm.commande = {};
					vm.preparation = {};
					vm.quantite = {};
					
					vm.row = [];
					
					// Récupération des types de préparations.
					CrudService.getAll('/typeDePreparation/').then(function(typePreparation) {
						vm.typePreparation = typePreparation;
					}, function(error) {
						vm.alerte.type = error.type;
						vm.alerte.texte = error.texte;
					});
					
					// Sélection des préparations.
					vm.selectionPreparation = function () {
						PreparationService.getByTypePreparation(vm.commande.typePreparation.libelle).then(function(preparation){
							vm.preparationListe = preparation;
						}, function(error) {
							vm.alerte.type = error.type;
							vm.alerte.texte = error.texte;
						});
					}
					
					// Méthode pour ajouter des préparation à la commande.
					vm.ajouterLigne = function () {
						var row = {
							typePreparation : vm.commande.typePreparation,
							preparation : vm.preparation,
							quantite : vm.quantite.quantite,
							prixCalcule : vm.quantite.prixCalcule,
						};

						if(vm.commande.quantites === undefined) {
							vm.commande.quantites = [];
						}
						vm.quantite.preparation = vm.preparation;
						vm.commande.quantites.push(row);
						if(vm.commande.prix === undefined) {
							vm.commande.prix = Number(0);
						} 
						vm.commande.prix = (Number(vm.commande.prix) + Number(vm.quantite.prixCalcule)).toFixed(2);
						
						// Ajouts des valeurs du controller courant (Modal) dans des variables statiques,
						// pour les récupérer depuis le controller d'édition des préparation.
						ModalChoixPreparationFactory.setQuantite(vm.quantite);
						ModalChoixPreparationFactory.setCommande(vm.commande);
						ModalChoixPreparationFactory.setRow(row);
					}
					
					// Permet de mettre à jour le prix en fonction de la quantité sélectionnée et de la préparation sélectionnée.
					vm.mettreAjourPrix = function() {
						var prix = vm.preparation.prix;
						// Faire opération.
						vm.quantite.prixCalcule = (prix * vm.quantite.quantite);
						
					}
					
				},
				controllerAs: 'vm',
				resolve : {
					result : function() {
							return;
					}
				}
			});
		}
		
		// Accesseurs.
		
		function getQuantite() {
			return this.quantite;
		}
		
		function setQuantite(_quantite) {
			this.quantite = _quantite;
		}
		
		function getCommande() {
			return this.commande;
		}
		
		function setCommande(_commande) {
			this.commande = _commande;
		}
		
		function getRow() {
			return this.row;
		}
		
		function setRow(_row) {
			this.row = _row;
		}
		
		return {
			// Fonctions.
			show: show,
			
			// Variables statiques.
			quantite : quantite,
			commande : commande,
			row : row,
			
			// Getters.
			getQuantite : getQuantite,
			getCommande : getCommande,
			getRow : getRow,
			
			// Setters
			setQuantite : setQuantite,
			setCommande : setCommande,
			setRow : setRow,
		};
		
	}
	
})();
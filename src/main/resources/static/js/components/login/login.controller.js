(function() {
	"use strict";

	/**
	 * Déclaration du module
	 */
	angular.module('LoginControllerModule', [
	// Modules injectés
	]).controller('LoginController', LoginController)

	/**
	 * Injection des dépendances
	 */
	LoginController.$inject = [ '$window', '$rootScope', '$scope', '$location', '$http' ];

	/**
	 * Implémentation du controleur
	 */
	function LoginController($window, $rootScope, $scope, $location, $http) {
		var self = this

		  var authenticate = function(credentials, callback) {

		    var headers = credentials ? {authorization : "Basic "
		        + btoa(credentials.username + ":" + credentials.password)
		    } : {};

		    $http.get('user', {headers : headers}).then(function(response) {
		      if (response.data.name) {
		        $rootScope.authenticated = true;
		      } else {
		        $rootScope.authenticated = false;
		      }
		      callback && callback();
		    }, function() {
		      $rootScope.authenticated = false;
		      callback && callback();
		    });

		  }

		  authenticate();
		  self.credentials = {};
		  self.login = function() {
		      authenticate(self.credentials, function() {
		        if ($rootScope.authenticated) {
		          $location.path("/");
		          self.error = false;
		        } else {
		          $location.path("/login");
		          self.error = true;
		        }
		      });
		  };

	}
})();
(function () {
	'use strict';

	/**
	 * Déclaration du module 
	 */
	angular
		.module('LoginRoutesModule', [
            // Modules injectés
		])
		.config(LoginRoutes);

	/**
	 * Définition des routes
	 * @param $routeProvider
	 * @constructor
	 */
	function LoginRoutes($routeProvider) {
		$routeProvider
		.when('/login', {
			templateUrl : 'js/components/login/login.directive.html',
			controller : 'LoginController',
			controllerAs : 'vm'
		});
	}

})();
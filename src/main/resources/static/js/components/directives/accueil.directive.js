(function () {
    "use strict";   
	
    /**
	 * Déclaration du module 
	 */
	angular
		.module('AccueilDirectiveModule', [
		    // Modules injectés
		])
		.controller('AccueilDirectiveController', AccueilDirectiveController)
		.directive('boulangerieHeader', BoulangerieHeader);
		
	/**
	 * Injection des dépendances
	 */
	AccueilDirectiveController.$inject = ['$window', '$rootScope', '$scope', '$location'];
	
	/**
	 * Implémentation du controleur
	 */
	function AccueilDirectiveController($window, $rootScope, $scope, $location) {
		var vm = this;
		
		/**
		 * Initialisation du modèle
		 */
		
		init();
		
		function init () {
		}
		
	}
	
	/**
	 * Implémentation de la directive
	 * @returns La directive
	 */
	function BoulangerieAccueil() {
		return {
			restrict: 'E',
			scope: true,
		    templateUrl: 'js/components/directives/accueil.directive.html',
		    controller: 'AcceuilDirectiveController',
		    controllerAs: 'vm'
		};
	}
	
})();
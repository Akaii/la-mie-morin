(function () {
    "use strict";   
	
    /**
	 * Déclaration du module 
	 */
	angular
		.module('HeaderDirectiveModule', [
		    // Modules injectés
		])
		.controller('HeaderDirectiveController', HeaderDirectiveController)
		.directive('boulangerieHeader', BoulangerieHeader);
		
	/**
	 * Injection des dépendances
	 */
	HeaderDirectiveController.$inject = ['$window', '$rootScope', '$scope', '$location'];
	
	/**
	 * Implémentation du controleur
	 */
	function HeaderDirectiveController($window, $rootScope, $scope, $location) {
		var vm = this;
		
		vm.collapseMenu = collapseMenu;
		/**
		 * Initialisation du modèle
		 */
		
		init();
		
		function init () {
		}
		
		$scope.isCollapsed = true;
		function NavBarCtrl($scope) {
		}
		
		vm.navClass = function (page) {
	        var currentRoute = $location.path().substring(1) || 'index';
	        return page === currentRoute ? 'active' : '';
	    }; 
	    
	    vm.liste = [{
	    	nom : 'Accueil',
	    	url : '#/index.html',
	    	icon : 'glyphicon glyphicon-home'
	    },{
	    	nom : 'Clients',
	    	url : '#/client/liste',
	    	icon : 'glyphicon glyphicon-phone-alt'
	    },{
	    	nom : 'Commandes',
	    	url : '#/commande/liste',
	    	icon : 'glyphicon glyphicon-list-alt'
	    },{
	    	nom : 'Préparations',
	    	url : '#/preparation/liste',
	    	icon : 'glyphicon glyphicon-tags'
	    },{
	    	nom : 'Calendrier',
	    	url : '#/recherche',
	    	icon : 'glyphicon glyphicon-calendar'
	    }];
	    
	    function collapseMenu() {
	    	$scope.isCollapsed = !$scope.isCollapsed;
	    }
	}
	
	/**
	 * Implémentation de la directive
	 * @returns La directive
	 */
	function BoulangerieHeader() {
		return {
			restrict: 'E',
			scope: true,
		    templateUrl: 'js/components/directives/header.directive.html',
		    controller: 'HeaderDirectiveController',
		    controllerAs: 'vm'
		};
	}
	
})();
package fr.morin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import fr.morin.controller.Controller;
import fr.morin.model.Entity;
import fr.morin.persistance.Repository;
import fr.morin.service.Service;

@SpringBootApplication
@ComponentScan(basePackageClasses = { Controller.class, Service.class })
@EntityScan(basePackageClasses = Entity.class)
@EnableJpaRepositories(basePackageClasses = Repository.class)
@EnableTransactionManagement
public class ApplicationWebEmbedded {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(ApplicationWebEmbedded.class);
	}
	
	@Configuration
	public class MvcConfig extends WebMvcConfigurerAdapter {

	    @Override
	    public void addViewControllers(ViewControllerRegistry registry) {
	        registry.addViewController("/login").setViewName("login");
	    }

	}
	
	@Configuration
	@EnableWebSecurity
	public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
		
		@Autowired
	    private UserDetailsService userDetailsService;
		
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {
	        http.headers().frameOptions().disable().and()
	            .authorizeRequests()
	                .antMatchers("/login", "/img/**").permitAll()
	                .anyRequest().authenticated()
	                .and()
	            .formLogin()
	                .loginPage("/login")
	                .permitAll()
	                .and()
	            .logout()
	                .permitAll().and().csrf().disable();
	    }

//	    @Autowired
//	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
//	        auth
//	            .inMemoryAuthentication()
//	                .withUser("user").password("password").roles("USER");
//	    }
	    
	    @Autowired
	    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(userDetailsService);
	    }
	}
}
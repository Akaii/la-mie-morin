package fr.morin.metier;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.morin.model.Commande;
import fr.morin.model.CommandeExportObjet;
import fr.morin.model.Quantite;

public class ExporteurCommandes {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExporteurCommandes.class);
	
	private static final String[] TITRES = {"Numéro de commande","Date de commande", "Date de livraison", "Client", "Statut de la commande", "Quantité, préparation : prix"};
	private static final String[] TITRES_COMPTEUR_PREPARATION_PAR_JOUR = {"Date de livraison", "Type de préparation", "Nom de la préparation", "Quantité totale"};
	
	
	public static void exporter(List<Commande> commandes, HttpServletResponse response) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String today = simpleDateFormat.format(Calendar.getInstance().getTime());
		
		Integer numeroLigne = new Integer(1);

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Commandes " + today);
		XSSFRow rowTitres = sheet.createRow(0);
		for(int i = 0; i < TITRES.length; i++) {
			rowTitres.createCell(i).setCellValue(TITRES[i]);
		}
		
		for(Commande commande : commandes) {
			// Create a row and put some cells in it. Rows are 0 based.
			XSSFRow row = sheet.createRow(numeroLigne);
	
			// Create a cell
			row.createCell(0).setCellValue(commande.getNumCommande());
			row.createCell(1).setCellValue(simpleDateFormat.format(commande.getDateCommande()));
			row.createCell(2).setCellValue(simpleDateFormat.format(commande.getDateLivraison()) + " " + commande.getHeureLivraison());
			
			if(commande.getClient() != null) {
				StringBuilder nomPrenom = new StringBuilder(32);
				nomPrenom.append(commande.getClient().getNom() == null ? "" : commande.getClient().getNom());
				nomPrenom.append(" ");
				nomPrenom.append(commande.getClient().getPrenom() == null ? "" : commande.getClient().getPrenom());
				row.createCell(3).setCellValue(nomPrenom.toString());
			}
			row.createCell(4).setCellValue(commande.getPaye() != null && commande.getPaye() == true ? "Payé" : "Non payé");
			
			StringBuilder builder = new StringBuilder(32);
			Float prixGlobalCommande = new Float(0);
			DecimalFormat df = new DecimalFormat("#.##");
			for(Quantite quantite : commande.getQuantites()) {
				System.out.println("Quantite : " + quantite.getId());
				Float prixCommande = quantite.getPreparation().getPrix() * quantite.getQuantite();
				builder.append(quantite.getQuantite());
				builder.append(", ");
				builder.append(quantite.getPreparation().getNomPreparation());
				builder.append(" : ");
				builder.append(prixCommande);
				builder.append(" €");
				builder.append("\n");
				prixGlobalCommande += prixCommande;
			}
			builder.append("\n");
			builder.append("Total : " + df.format(prixGlobalCommande) + " €");
			XSSFCell cell = row.createCell(5);
			
			// Ajout d'un style pour que la cellule saute automatiquement les lignes à l'affichage.
			XSSFCellStyle style = wb.createCellStyle();
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_JUSTIFY);
			style.setWrapText(true);
			
			cell.setCellStyle(style);			
			cell.setCellValue(builder.toString());
			numeroLigne ++;
		}
		// Write the output to a file
		
		for(int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
		
		try {
			wb.write(response.getOutputStream());
		} catch (Exception e) {
			LOGGER.error("Une erreur est survenue lors de l'export du fichier de commandes.", e);
		}
	}
	
	public static void exporterCompteurPreparation(Collection<CommandeExportObjet> commandes, HttpServletResponse response) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String today = simpleDateFormat.format(Calendar.getInstance().getTime());
		
		Integer numeroLigne = new Integer(1);

		XSSFWorkbook wb = new XSSFWorkbook();
		XSSFSheet sheet = wb.createSheet("Commandes " + today);
		XSSFRow rowTitres = sheet.createRow(0);
		for(int i = 0; i < TITRES_COMPTEUR_PREPARATION_PAR_JOUR.length; i++) {
			rowTitres.createCell(i).setCellValue(TITRES_COMPTEUR_PREPARATION_PAR_JOUR[i]);
		}
		
		for(CommandeExportObjet commande : commandes) {
			// Create a row and put some cells in it. Rows are 0 based.
			XSSFRow row = sheet.createRow(numeroLigne);
	
			// Create a cell
			row.createCell(0).setCellValue(simpleDateFormat.format((commande.getDateLivraison())));
			row.createCell(1).setCellValue(commande.getNomPreparation());
			row.createCell(2).setCellValue(commande.getLibellePreparation());
			row.createCell(3).setCellValue(commande.getQuantite());
			
			
			// Ajout d'un style pour que la cellule saute automatiquement les lignes à l'affichage.
			XSSFCellStyle style = wb.createCellStyle();
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_JUSTIFY);
			style.setWrapText(true);
			
			numeroLigne ++;
		}
		// Write the output to a file
		
		for(int i = 0; i < sheet.getRow(0).getLastCellNum(); i++) {
			sheet.autoSizeColumn(i);
		}
		
		try {
			wb.write(response.getOutputStream());
		} catch (Exception e) {
			LOGGER.error("Une erreur est survenue lors de l'export du fichier de commandes.", e);
		}
	}
}

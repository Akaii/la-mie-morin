package fr.morin.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.view.View;

@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Client {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView( {View.Commande.class, View.Client.class} )
	private Long id;
	
	@JsonView( {View.Commande.class, View.Client.class} )
	private String nom;
	
	@JsonView( {View.Commande.class, View.Client.class} )
	private String prenom;
	
	@JsonView( {View.Commande.class, View.Client.class} )
	private String numero;
	
	@OneToMany(mappedBy="client", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonView( {View.Client.class} )
	private Set<Commande> commande;

	@OneToMany(mappedBy="client", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	@JsonView( {View.Commande.class} )
	private List<HistoriqueCommande> historiqueCommande;
	
	public Client() {
	}

	@Override
	public String toString() {
		return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ", commande=" + commande + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public Set<Commande> getCommande() {
		return commande;
	}

	public void setCommande(Set<Commande> commande) {
		this.commande = commande;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

}

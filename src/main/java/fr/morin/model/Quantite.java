package fr.morin.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.view.View;
import fr.morin.model.view.View.CommandeQuantite;

@Entity
public class Quantite {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView( {View.Quantite.class, CommandeQuantite.class} )
	private Long id;
	
	@JsonView( {View.Quantite.class, CommandeQuantite.class} )
	private Integer quantite;
	
	@Column(name = "prix_calcule")
	@JsonView( {View.Quantite.class, CommandeQuantite.class} )
	private Float prixCalcule;

	@ManyToOne(cascade = CascadeType.REMOVE)
	@JoinColumn(name = "preparation")
	@JsonView( {View.Quantite.class, CommandeQuantite.class} )
	private Preparation preparation;
	
	public Quantite() {
	}

	@Override
	public String toString() {
		return "Quantite [id=" + id + ", quantite=" + quantite + ", prixCalcule=" + prixCalcule + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	public Float getPrixCalcule() {
		return prixCalcule;
	}

	public void setPrixCalcule(Float prixCalcule) {
		this.prixCalcule = prixCalcule;
	}

	public Preparation getPreparation() {
		return preparation;
	}

	public void setPreparation(Preparation preparation) {
		this.preparation = preparation;
	}
	
}

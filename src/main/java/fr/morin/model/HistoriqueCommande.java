package fr.morin.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.view.View;

@Entity
@Table(name = "Historique_commande")
public class HistoriqueCommande {

	@Id
	@Column(name = "numero_commande")
	private Long numeroCommande;
	
	@OneToMany(mappedBy="historiqueCommande", fetch = FetchType.LAZY, cascade=CascadeType.ALL)
	private Set<Commande> commande;
	
	@ManyToOne
	@JoinColumn(name = "client")
	private Client client;

	public HistoriqueCommande() {
	}

	public Long getNumeroCommande() {
		return numeroCommande;
	}

	public void setNumeroCommande(Long numeroCommande) {
		this.numeroCommande = numeroCommande;
	}



	public Set<Commande> getCommande() {
		return commande;
	}

	public void setCommande(Set<Commande> commande) {
		this.commande = commande;
	}

	@Override
	public String toString() {
		return "HistoriqueCommande [numeroCommande=" + numeroCommande + ", commande=" + commande + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((commande == null) ? 0 : commande.hashCode());
		result = prime * result + ((numeroCommande == null) ? 0 : numeroCommande.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HistoriqueCommande other = (HistoriqueCommande) obj;
		if (commande == null) {
			if (other.commande != null)
				return false;
		} else if (!commande.equals(other.commande))
			return false;
		if (numeroCommande == null) {
			if (other.numeroCommande != null)
				return false;
		} else if (!numeroCommande.equals(other.numeroCommande))
			return false;
		return true;
	}
	
}

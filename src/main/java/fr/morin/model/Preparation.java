package fr.morin.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.view.View;

@Entity
public class Preparation {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
	@JsonView( {View.Preparation.class, View.CommandeQuantitePreparation.class} )
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "type_preparation")
	@JsonView( {View.Preparation.class, View.CommandeQuantitePreparation.class} )
	private TypeDePreparation typePreparation;
	
	@JsonView( {View.Preparation.class, View.CommandeQuantitePreparation.class} )
	private String nomPreparation;
	
	@JsonView( {View.Preparation.class, View.CommandeQuantitePreparation.class} )
	private Float prix;
	
	@OneToMany(mappedBy="preparation", fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
	private List<Quantite> quantite;
	
	public Preparation() {
	}

	@Override
	public String toString() {
		return "Preparation [id=" + id + ", typePreparation=" + typePreparation + ", nomPreparation=" + nomPreparation
				+ ", prix=" + prix + ", quantite=" + quantite + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeDePreparation getTypePreparation() {
		return typePreparation;
	}

	public void setTypePreparation(TypeDePreparation typePreparation) {
		this.typePreparation = typePreparation;
	}

	public String getNomPreparation() {
		return nomPreparation;
	}

	public void setNomPreparation(String nomPreparation) {
		this.nomPreparation = nomPreparation;
	}

	public Float getPrix() {
		return prix;
	}

	public void setPrix(Float prix) {
		this.prix = prix;
	}

	public List<Quantite> getQuantite() {
		return quantite;
	}

	public void setQuantite(List<Quantite> quantite) {
		this.quantite = quantite;
	}
	
}

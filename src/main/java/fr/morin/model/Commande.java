package fr.morin.model;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.view.View;

@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property="@UUID")
public class Commande {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "numero_commande")
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	private Long numeroCommande;
	
	@Column(name = "num_commande")
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	private String numCommande;
	
	@Column(name = "date_commande")
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "Europe/Paris")
	private Date dateCommande;
	
	@Column(name = "date_livraison")
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS", timezone = "Europe/Paris")
	private Date dateLivraison;
	
	@Column(name = "heure_livraison")
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	private String heureLivraison;
	
	@Column(name = "paye", columnDefinition = "boolean default false", nullable = false)
	@JsonView( {View.Commande.class, View.ClientCommande.class} )
	private Boolean paye;
	
	@JsonView( {View.Commande.class} )
	private Float prix;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade={CascadeType.PERSIST, CascadeType.REMOVE})
	@JoinTable(name = "commande_quantite",
    joinColumns = @JoinColumn(name = "numero_commande", referencedColumnName = "numero_commande"),
    inverseJoinColumns = @JoinColumn(name = "id", referencedColumnName = "id"))
	@JsonView( {View.CommandeQuantite.class} )
	private Set<Quantite> quantites;
	
	@ManyToOne
	@JoinColumn(name = "historiqueCommande")
	@JsonView( {View.Commande.class} )
	private HistoriqueCommande historiqueCommande;
	
	@ManyToOne
	@JoinColumn(name = "client")
	@JsonView( {View.Commande.class} )
	private Client client;
	
	@JsonView( {View.Commande.class} )
	private Boolean receptionne;
	
	public Commande() {
	}

	@Override
	public String toString() {
		return "Commande [numeroCommande=" + numeroCommande + ", dateCommande=" + dateCommande + ", dateLivraison="
				+ dateLivraison + ", paye=" + paye + ", prix=" + prix + ", quantites=" + quantites
				+ ", historiqueCommande=" + historiqueCommande + ", client=" + client + "]";
	}

	public Boolean getReceptionne() {
		return receptionne;
	}

	public void setReceptionne(Boolean receptionne) {
		this.receptionne = receptionne;
	}

	public String getHeureLivraison() {
		return heureLivraison;
	}

	public void setHeureLivraison(String heureLivraison) {
		this.heureLivraison = heureLivraison;
	}

	public Long getNumeroCommande() {
		return numeroCommande;
	}

	public void setNumeroCommande(Long numeroCommande) {
		this.numeroCommande = numeroCommande;
	}

	public Date getDateCommande() {
		return dateCommande;
	}

	public void setDateCommande(Date dateCommande) {
		this.dateCommande = dateCommande;
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	public Boolean getPaye() {
		return paye;
	}

	public void setPaye(Boolean paye) {
		this.paye = paye;
	}

	public Float getPrix() {
		return prix;
	}

	public void setPrix(Float prix) {
		this.prix = prix;
	}

	public Set<Quantite> getQuantites() {
		return quantites;
	}

	public void setQuantites(Set<Quantite> quantites) {
		this.quantites = quantites;
	}

	public HistoriqueCommande getHistoriqueCommande() {
		return historiqueCommande;
	}

	public void setHistoriqueCommande(HistoriqueCommande historiqueCommande) {
		this.historiqueCommande = historiqueCommande;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public String getNumCommande() {
		return numCommande;
	}

	public void setNumCommande(String numCommande) {
		this.numCommande = numCommande;
	}

}

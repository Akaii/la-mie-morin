package fr.morin.model.view;

public class View {
	public interface Client {};
	public interface ClientCommande extends Client {};
	
	public interface Commande {};
	public interface CommandeQuantite extends Commande {};
	public interface CommandeQuantitePreparation extends CommandeQuantite {};

	public interface Quantite {};
	
	public interface Preparation {};
	
	public interface User {};
}
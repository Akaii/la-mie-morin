package fr.morin.model;

import java.util.Date;

public class CommandeExportObjet {
	
	private Date dateLivraison;
	
	private String nomPreparation;
	
	private String libellePreparation;
	
	private Integer quantite;

	@Override
	public String toString() {
		return "CommandeExportObjet [dateLivraison=" + dateLivraison + ", nomPreparation=" + nomPreparation
				+ ", libellePreparation=" + libellePreparation + ", quantite=" + quantite + "]";
	}

	public Date getDateLivraison() {
		return dateLivraison;
	}

	public void setDateLivraison(Date dateLivraison) {
		this.dateLivraison = dateLivraison;
	}

	public String getNomPreparation() {
		return nomPreparation;
	}

	public void setNomPreparation(String nomPreparation) {
		this.nomPreparation = nomPreparation;
	}

	public String getLibellePreparation() {
		return libellePreparation;
	}

	public void setLibellePreparation(String libellePreparation) {
		this.libellePreparation = libellePreparation;
	}

	public Integer getQuantite() {
		return quantite;
	}

	public void setQuantite(Integer quantite) {
		this.quantite = quantite;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateLivraison == null) ? 0 : dateLivraison.hashCode());
		result = prime * result + ((libellePreparation == null) ? 0 : libellePreparation.hashCode());
		result = prime * result + ((nomPreparation == null) ? 0 : nomPreparation.hashCode());
		result = prime * result + ((quantite == null) ? 0 : quantite.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CommandeExportObjet other = (CommandeExportObjet) obj;
		if (dateLivraison == null) {
			if (other.dateLivraison != null)
				return false;
		} else if (!dateLivraison.equals(other.dateLivraison))
			return false;
		if (libellePreparation == null) {
			if (other.libellePreparation != null)
				return false;
		} else if (!libellePreparation.equals(other.libellePreparation))
			return false;
		if (nomPreparation == null) {
			if (other.nomPreparation != null)
				return false;
		} else if (!nomPreparation.equals(other.nomPreparation))
			return false;
		if (quantite == null) {
			if (other.quantite != null)
				return false;
		} else if (!quantite.equals(other.quantite))
			return false;
		return true;
	}
	
}

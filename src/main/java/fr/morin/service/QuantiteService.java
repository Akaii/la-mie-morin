package fr.morin.service;

import fr.morin.model.Quantite;

public interface QuantiteService extends GenericService<Quantite, Long> {

}

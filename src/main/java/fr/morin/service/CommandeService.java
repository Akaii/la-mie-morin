package fr.morin.service;

import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import fr.morin.model.Commande;

public interface CommandeService extends GenericService<Commande, Long> {

	public Iterable<Commande> getAllPageable(Integer page, Integer offset, String numeroCommande, String client, String dateLivraison, String sortType, String sortReverse);
	
	public Iterable<Commande> getByDateCurrentMonth(Date date);
	
	public Long count();
	
	public void exporter(HttpServletResponse response, String numeroCommande, String client, String _dateLivraison);
	
	public void countPreparationCommande(HttpServletResponse response, String numeroCommande, String client, String _dateLivraison);
}

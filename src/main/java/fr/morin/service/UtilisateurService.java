package fr.morin.service;

import java.util.List;

import fr.morin.model.Client;

public interface UtilisateurService extends GenericService<Client, Long> {

	public List<Client> getAllSearch(String nom);
}

package fr.morin.service;

import java.io.Serializable;

public interface GenericService<T, ID extends Serializable> {

	T get(ID pk);
	
	Iterable<T> getAll();
	
	T create(T entity);
	
	T update(T entity);
	
	void delete(ID pk);
}

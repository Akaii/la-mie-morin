package fr.morin.service;

import fr.morin.model.authtent.User;

public interface UserService {
	
	public User findByUsername(String username);
}

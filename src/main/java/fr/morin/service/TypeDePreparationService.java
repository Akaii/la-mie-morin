package fr.morin.service;

import fr.morin.model.TypeDePreparation;

public interface TypeDePreparationService extends GenericService<TypeDePreparation, Long> {

}

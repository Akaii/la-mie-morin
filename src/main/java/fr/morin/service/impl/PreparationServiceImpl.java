package fr.morin.service.impl;

import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

import fr.morin.model.Preparation;
import fr.morin.model.Quantite;
import fr.morin.persistance.PreparationRepository;
import fr.morin.service.PreparationService;

@Component
@Transactional
public class PreparationServiceImpl implements PreparationService {

	private @Autowired PreparationRepository preparationRepository;
	private @Autowired EntityManager em;

	@Override
	public Preparation get(Long pk) {
		return preparationRepository.findOne(pk);
	}

	@Override
	public Iterable<Preparation> getAll() {
		return preparationRepository.findAllByOrderByNomPreparationAsc();
	}

	@Override
	public Preparation create(Preparation entity) {
		return preparationRepository.save(entity);
	}

	@Override
	public Preparation update(Preparation entity) {
		return preparationRepository.save(entity);
	}

	@Override
	public void delete(Long pk) {
		
		Preparation preparation = preparationRepository.findOne(pk);
		Set<Quantite> quantites = Sets.newHashSet(preparation.getQuantite());
//		quantiteRepository.delete(quantites);
		for(Quantite quantite : quantites) {
			String query = "DELETE FROM commande_quantite WHERE id=?1";
			Query q = em.createNativeQuery(query);
	        q.setParameter(1,quantite.getId());
	        q.executeUpdate();
		}
        preparationRepository.delete(pk);
	}
	
	public Iterable<Preparation> getByType(String type) {
		return preparationRepository.getByType(type);
	}
	
}

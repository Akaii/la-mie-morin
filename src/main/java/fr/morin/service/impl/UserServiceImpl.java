package fr.morin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.morin.model.authtent.User;
import fr.morin.persistance.UserRepository;
import fr.morin.service.UserService;

@Service
public class UserServiceImpl implements UserService {
	
    @Autowired
    private UserRepository userRepository;
    
    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}

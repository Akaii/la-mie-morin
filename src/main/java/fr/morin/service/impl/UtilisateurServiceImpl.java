package fr.morin.service.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.morin.model.Client;
import fr.morin.model.Commande;
import fr.morin.persistance.CommandeRepository;
import fr.morin.persistance.QuantiteRepository;
import fr.morin.persistance.UtilisateurRepository;
import fr.morin.service.UtilisateurService;

@Component
@Transactional
public class UtilisateurServiceImpl implements UtilisateurService {

	private @Autowired UtilisateurRepository utilisateurRepository;
	private @Autowired CommandeRepository commandeRepository;
	private @Autowired EntityManager em;

	@Override
	public Client get(Long pk) {
		return utilisateurRepository.findOne(pk);
	}

	@Override
	public Iterable<Client> getAll() {
		return utilisateurRepository.findAllByOrderByNomAsc();
	}

	@Override
	public Client create(Client entity) {
		return utilisateurRepository.save(entity);
	}

	@Override
	public Client update(Client entity) {
		return utilisateurRepository.save(entity);
	}

	@Override
	public void delete(Long pk) {
		Client client = utilisateurRepository.findOne(pk);
		for(Commande commande : client.getCommande()) {
			String query = "DELETE FROM commande_quantite WHERE numero_commande=?1";
			Query q = em.createNativeQuery(query);
			q.setParameter(1,commande.getNumeroCommande());
			q.executeUpdate();
			commandeRepository.delete(commande.getNumeroCommande());
		}
		utilisateurRepository.delete(pk);
	}
	
	@Override
	public List<Client> getAllSearch(String nom) {
		return (List<Client>) utilisateurRepository.findAllByNomContainingOrderByNomAsc(nom);
	}
	
}

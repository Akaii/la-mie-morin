package fr.morin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.morin.model.TypeDePreparation;
import fr.morin.persistance.TypeDePreparationRepository;
import fr.morin.service.TypeDePreparationService;

@Component
@Transactional
public class TypeDePreparationServiceImpl implements TypeDePreparationService {

	private @Autowired TypeDePreparationRepository typeDePreparationRepository;

	@Override
	public TypeDePreparation get(Long pk) {
		return typeDePreparationRepository.findOne(pk);
	}

	@Override
	public Iterable<TypeDePreparation> getAll() {
		return typeDePreparationRepository.findAll();
	}

	@Override
	public TypeDePreparation create(TypeDePreparation entity) {
		return typeDePreparationRepository.save(entity);
	}

	@Override
	public TypeDePreparation update(TypeDePreparation entity) {
		return typeDePreparationRepository.save(entity);
	}

	@Override
	public void delete(Long pk) {
		typeDePreparationRepository.delete(pk);
	}
	
}

package fr.morin.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;

import fr.morin.metier.ExporteurCommandes;
import fr.morin.model.Commande;
import fr.morin.model.CommandeExportObjet;
import fr.morin.model.Quantite;
import fr.morin.persistance.CommandeRepository;
import fr.morin.persistance.QuantiteRepository;
import fr.morin.service.CommandeService;

@Component
@Transactional
@SuppressWarnings("rawtypes")
public class CommandeServiceImpl implements CommandeService {

	private @Autowired CommandeRepository commandeRepository;
	private @Autowired QuantiteRepository quantiteRepository;
	private @Autowired EntityManager em;
	
	/** Nombre de pages totale. */
	private static Integer PAGE_TOTALE;
	
	/** Nombre d'enregistrements total. */
	private static Long ENREGISTREMENT_TOTAL;
	
	private Logger LOGGER = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public Commande get(Long pk) {
		return commandeRepository.findOne(pk);
	}

	@Override
	public Iterable<Commande> getAll() {
		return commandeRepository.findAll(new PageRequest(0, 5)).getContent();
	}

	@Override
	public Commande create(Commande entity) {
		return commandeRepository.save(entity);
	}

	@Override
	public Commande update(Commande commande) {
		Set<Quantite> quantites = commande.getQuantites();
		commande.setQuantites(Sets.newHashSet(quantiteRepository.save(quantites)));
		return commandeRepository.save(commande);
	}

	@Override
	public void delete(Long pk) {
		Commande commande = commandeRepository.getOne(pk);
		String query = "DELETE FROM commande_quantite WHERE numero_commande=?1";
		Query q = em.createNativeQuery(query);
        q.setParameter(1,pk);
        q.executeUpdate();
        Set<Quantite> quantites = Sets.newHashSet(commande.getQuantites());
        quantiteRepository.delete(quantites);
		commandeRepository.delete(pk);
	}

	@Override
	public Iterable<Commande> getAllPageable(Integer page, Integer offset, String numeroCommande, String client, String _dateLivraison, String sortType, String sortReverse) {
		Page<Commande> pages;
		if(_dateLivraison != null) {
			Date dateLivraison;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			try {
				dateLivraison = sdf.parse(_dateLivraison);
			} catch (ParseException e) {
				LOGGER.error("Une erreur est survenue lors du parsing de la date.", e);
				throw new RuntimeException(e);
			}
			
			pages = commandeRepository.findAllByNumCommandeContainingAndClientNomContainingAndDateLivraison(new PageRequest(page, offset, new Sort(new Order(sortReverse.equals("true") ? Direction.DESC : Direction.ASC , sortType))), numeroCommande, client, dateLivraison);
		} else {
			pages = commandeRepository.findAllByNumCommandeContainingAndClientNomContaining(new PageRequest(page, offset, new Sort(new Order(sortReverse.equals("true") ? Direction.DESC : Direction.ASC, sortType))), numeroCommande, client);
		}
		ENREGISTREMENT_TOTAL = pages.getTotalElements();
		PAGE_TOTALE = pages.getTotalPages();
		return pages.getContent();
	}
	
	public Iterable<Commande> getByDateCurrentMonth(Date date) {
		return commandeRepository.findByDate(date);
	}
	
	@Override
	public Long count() {
		return ENREGISTREMENT_TOTAL;
	}

	@Override
	public void exporter(HttpServletResponse response, String numeroCommande, String client, String _dateLivraison) {
		List<Commande> commandes;
		if(_dateLivraison != null) {
			Date dateLivraison;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			try {
				dateLivraison = sdf.parse(_dateLivraison);
			} catch (ParseException e) {
				LOGGER.error("Une erreur est survenue lors du parsing de la date.", e);
				throw new RuntimeException(e);
			}
			
			commandes = (List<Commande>) commandeRepository.findAllByNumCommandeContainingAndClientNomContainingAndDateLivraison(numeroCommande, client, dateLivraison);
		} else {
			commandes = (List<Commande>) commandeRepository.findAllByNumCommandeContainingAndClientNomContaining(numeroCommande, client);
		}
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String today = simpleDateFormat.format(Calendar.getInstance().getTime());
		
		response.addHeader("content-disposition", "attachment; filename=Commandes_" + today + ".xlsx");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");

		ExporteurCommandes.exporter(commandes, response);
		
		try {
			response.flushBuffer();
		} catch (IOException e) {
			LOGGER.error("Une erreur est survenu lors de la fermeture de la réponse.", e);
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void countPreparationCommande(HttpServletResponse response, String numeroCommande, String client, String _dateLivraison) {

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String today = simpleDateFormat.format(Calendar.getInstance().getTime());
		
		response.addHeader("content-disposition", "attachment; filename=Preparations_" + today + ".xlsx");
		response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
		
		String query = "select c.date_livraison, p.nom_preparation, tdp.libelle, "
				+ "("
					+ "SELECT "
					+ "SUM(quantite) from commande "
					+ "group by q.preparation, str_to_date(c.date_livraison, '%Y-%m-%d') "
				+ ") as QuantiteTotale "
				+ "from commande as c "
				+ "INNER JOIN commande_quantite as cq ON c.numero_commande = cq.numero_commande "
				+ "INNER JOIN quantite as q ON q.id = cq.id "
				+ "INNER JOIN preparation as p ON p.id = q.preparation "
				+ "INNER JOIN type_de_preparation as tdp ON p.type_preparation = tdp.id "
				+ "INNER JOIN client as cl ON c.client = cl.id "
				+ "where q.id in(cq.id) and c.numero_commande in(cq.numero_commande) "
				+ "and c.num_commande LIKE ?1 and cl.nom LIKE ?2 and ?3 is null or c.date_livraison = ?3 "
				+ "group by p.id, str_to_date(c.date_livraison, '%Y-%m-%d') "
				+ "order by c.date_livraison, tdp.libelle";
		Query q = em.createNativeQuery(query);

		q.setParameter(1, "%" + numeroCommande + "%");
		q.setParameter(2, "%" + client + "%");
		q.setParameter(3, _dateLivraison);
		
		CommandeExportObjet commandeExportObjet;
		ArrayList<CommandeExportObjet> commandeExportObjets= new ArrayList<>();
		Iterator iterator = q.getResultList().iterator();
		    while(iterator.hasNext()){
		        Object[] obj= (Object[]) iterator.next();
		        commandeExportObjet= new CommandeExportObjet();
		        commandeExportObjet.setDateLivraison((Date)obj[0]);
		        commandeExportObjet.setNomPreparation((String)obj[1]);
		        commandeExportObjet.setLibellePreparation((String)obj[2]);
		        commandeExportObjet.setQuantite(((BigDecimal)obj[3]).intValue());
		        commandeExportObjets.add(commandeExportObjet);
		    }
		
		ExporteurCommandes.exporterCompteurPreparation(commandeExportObjets, response);
	}

}

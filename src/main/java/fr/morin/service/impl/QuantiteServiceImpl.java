package fr.morin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import fr.morin.model.Quantite;
import fr.morin.persistance.QuantiteRepository;
import fr.morin.service.QuantiteService;

@Component
@Transactional
public class QuantiteServiceImpl implements QuantiteService {

	private @Autowired QuantiteRepository quatiteRepository;

	@Override
	public Quantite get(Long pk) {
		return quatiteRepository.findOne(pk);
	}

	@Override
	public Iterable<Quantite> getAll() {
		return quatiteRepository.findAll();
	}

	@Override
	public Quantite create(Quantite entity) {
		return quatiteRepository.save(entity);
	}

	@Override
	public Quantite update(Quantite entity) {
		return quatiteRepository.save(entity);
	}

	@Override
	public void delete(Long pk) {
		quatiteRepository.delete(pk);
	}

	
}

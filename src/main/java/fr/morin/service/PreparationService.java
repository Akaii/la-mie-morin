package fr.morin.service;

import fr.morin.model.Preparation;

public interface PreparationService extends GenericService<Preparation, Long> {

	Iterable<Preparation> getByType(String type);
}

package fr.morin.persistance;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import fr.morin.model.Commande;

@Repository
public interface CommandeRepository extends JpaRepository<Commande, Long> {

	@Query("SELECT c FROM Commande c WHERE c.dateLivraison > ?1")
	public Iterable<Commande> findByDate(Date date);
	
	public Page<Commande> findAllByNumCommandeContainingAndClientNomContainingAndDateLivraison(Pageable pageable, String numCommande, String clients, Date dateLivraison);
	
	public List<Commande> findAllByNumCommandeContainingAndClientNomContainingAndDateLivraison(String numCommande, String clients, Date dateLivraison);
	
	public Page<Commande> findAllByNumCommandeContainingAndClientNomContaining(Pageable pageable, String numCommande, String clients);
	
	public List<Commande> findAllByNumCommandeContainingAndClientNomContaining(String numCommande, String clients);
	
}

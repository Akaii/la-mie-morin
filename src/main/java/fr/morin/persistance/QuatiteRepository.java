package fr.morin.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.morin.model.Quantite;

@Repository
public interface QuatiteRepository extends CrudRepository<Quantite, Long> {

}

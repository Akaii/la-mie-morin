package fr.morin.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.morin.model.TypeDePreparation;

@Repository
public interface TypeDePreparationRepository extends CrudRepository<TypeDePreparation, Long> {

}

package fr.morin.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.morin.model.authtent.User;

public interface UserRepository extends JpaRepository<User, Long> {

	User findByUsername(String username);
}

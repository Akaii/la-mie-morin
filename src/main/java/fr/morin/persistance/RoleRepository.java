package fr.morin.persistance;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.morin.model.authtent.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}

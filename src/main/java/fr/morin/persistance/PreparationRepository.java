package fr.morin.persistance;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import fr.morin.model.Preparation;

@Repository
public interface PreparationRepository extends CrudRepository<Preparation, Long> {

	@Query("SELECT p FROM Preparation p WHERE p.typePreparation = (select t from TypeDePreparation t where t.libelle = :nom) order by p.nomPreparation asc")
	public Iterable<Preparation> getByType(@Param("nom") String nom);
	
	public Iterable<Preparation> findAllByOrderByNomPreparationAsc();
}

package fr.morin.persistance;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import fr.morin.model.Client;

@Repository
public interface UtilisateurRepository extends CrudRepository<Client, Long> {

	public Iterable<Client> findAllByOrderByNomAsc();
	
	public Iterable<Client> findAllByNomContainingOrderByNomAsc(String nom);
}

package fr.morin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.Preparation;
import fr.morin.model.view.View;
import fr.morin.service.PreparationService;

@Controller
public class PreparationController {

	private @Autowired PreparationService preparationService;
	
	@RequestMapping(path = "/preparation", method=RequestMethod.GET)
	@JsonView(View.Preparation.class)
	public @ResponseBody List<Preparation> getAll() {
		return (List<Preparation>) preparationService.getAll();
	}
	
	@RequestMapping(path = "/preparation/{id}", method=RequestMethod.GET)
	@JsonView(View.Preparation.class)
	public @ResponseBody Preparation get(@PathVariable Long id) {
		return (Preparation) preparationService.get(id);
	}
	
	@RequestMapping(path = "/preparation/", method=RequestMethod.PUT)
	public @ResponseBody Preparation get(@RequestBody Preparation utilisateur) {
		return (Preparation) preparationService.update(utilisateur);
	}
	
	@RequestMapping(path = "/preparation", method=RequestMethod.POST)
	public @ResponseBody Preparation create(@RequestBody Preparation utilisteur) {
		return (Preparation) preparationService.create(utilisteur);
	}
	
	@RequestMapping(path = "/preparation/{id}", method=RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable Long id) {
		preparationService.delete(id);
	}
	
	@RequestMapping(path = "/preparation/getByType/{nom}", method=RequestMethod.GET)
	@JsonView(View.Preparation.class)
	public @ResponseBody Iterable<Preparation> getByType(@PathVariable String nom) {
		return preparationService.getByType(nom);
	}
	
}

package fr.morin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.Client;
import fr.morin.model.view.View;
import fr.morin.service.UtilisateurService;

@Controller
public class UtilisateurController {

	private @Autowired UtilisateurService utilisateurService;
	
	@RequestMapping(path = "/clients", method=RequestMethod.GET)
	@JsonView( {View.ClientCommande.class} )
	public @ResponseBody List<Client> getAll() {
		return (List<Client>) utilisateurService.getAll();
	}
	
	@RequestMapping(path = "/clients/recherche", method=RequestMethod.GET)
	@JsonView( {View.ClientCommande.class} )
	public @ResponseBody List<Client> getAllSearch(@RequestParam(value = "nom", required = false, defaultValue="") String nom) {
		return (List<Client>) utilisateurService.getAllSearch(nom);
	}
	
	@RequestMapping(path = "/client/{id}", method=RequestMethod.GET)
	@JsonView( {View.ClientCommande.class} )
	public @ResponseBody Client get(@PathVariable Long id) {
		return (Client) utilisateurService.get(id);
	}
	
	@RequestMapping(path = "/client", method=RequestMethod.PUT)
	@JsonView( {View.ClientCommande.class} )
	public @ResponseBody Client update(@RequestBody Client utilisateur) {
		return (Client) utilisateurService.update(utilisateur);
	}
	
	@RequestMapping(path = "/client", method=RequestMethod.POST)
	public @ResponseBody Client create(@RequestBody Client utilisteur) {
		return (Client) utilisateurService.create(utilisteur);
	}
	
	@RequestMapping(path = "/client/{id}", method=RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable Long id) {
		utilisateurService.delete(id);
	}
	
}

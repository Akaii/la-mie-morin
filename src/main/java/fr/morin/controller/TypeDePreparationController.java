package fr.morin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.morin.model.TypeDePreparation;
import fr.morin.service.TypeDePreparationService;

@Controller
public class TypeDePreparationController {

	private @Autowired TypeDePreparationService typeDePreparationService;
	
	@RequestMapping(path = "/typeDePreparation", method=RequestMethod.GET)
	public @ResponseBody List<TypeDePreparation> getAll() {
		return (List<TypeDePreparation>) typeDePreparationService.getAll();
	}
	
	@RequestMapping(path = "/typeDePreparation/{id}", method=RequestMethod.GET)
	public @ResponseBody TypeDePreparation get(@PathVariable Long id) {
		return (TypeDePreparation) typeDePreparationService.get(id);
	}
	
	@RequestMapping(path = "/typeDePreparation/{id}", method=RequestMethod.PUT)
	public @ResponseBody TypeDePreparation get(@RequestBody TypeDePreparation typeDePreparation) {
		return (TypeDePreparation) typeDePreparationService.update(typeDePreparation);
	}
	
	@RequestMapping(path = "/typeDePreparation", method=RequestMethod.POST)
	public @ResponseBody TypeDePreparation create(@RequestBody TypeDePreparation typeDePreparation) {
		return (TypeDePreparation) typeDePreparationService.create(typeDePreparation);
	}
	
	@RequestMapping(path = "/typeDePreparation/{id}", method=RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable Long id) {
		typeDePreparationService.delete(id);
	}
	
}

package fr.morin.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import fr.morin.model.Quantite;
import fr.morin.service.QuantiteService;

@Controller
public class QuantiteController {

	private @Autowired QuantiteService quantiteService;
	
	@RequestMapping(path = "/quantite", method=RequestMethod.GET)
	public @ResponseBody List<Quantite> getAll() {
		return (List<Quantite>) quantiteService.getAll();
	}
	
	@RequestMapping(path = "/quantite/{id}", method=RequestMethod.GET)
	public @ResponseBody Quantite get(@PathVariable Long id) {
		return (Quantite) quantiteService.get(id);
	}
	
	@RequestMapping(path = "/quantite/{id}", method=RequestMethod.PUT)
	public @ResponseBody Quantite get(@RequestBody Quantite quantite) {
		return (Quantite) quantiteService.update(quantite);
	}
	
	@RequestMapping(path = "/quantite", method=RequestMethod.POST)
	public @ResponseBody Quantite create(@RequestBody Quantite quantite) {
		return (Quantite) quantiteService.create(quantite);
	}
	
	@RequestMapping(path = "/quantite/{id}", method=RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable Long id) {
		quantiteService.delete(id);
	}
	
}

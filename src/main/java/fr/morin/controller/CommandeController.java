package fr.morin.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.annotation.JsonView;

import fr.morin.model.Commande;
import fr.morin.model.view.View;
import fr.morin.service.CommandeService;

@Controller
public class CommandeController {

	private @Autowired CommandeService commandeService;
	
	@RequestMapping(path = "/commande/", method=RequestMethod.GET)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody List<Commande> getAll() {
		return (List<Commande>) commandeService.getAll();
	}
	
	@RequestMapping(path = "/commande/pagine/{page}/{offset}", method=RequestMethod.GET)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody List<Commande> getAllPageable(
			@PathVariable Integer page,
			@PathVariable Integer offset,
			@RequestParam(value = "numeroCommande", required = false, defaultValue="") String numeroCommande,
			@RequestParam(value = "client", required = false, defaultValue="") String client,
			@RequestParam(value = "dateLivraison", required = false) String dateLivraison,
			@RequestParam(value = "sortType", required = false) String sortType,
			@RequestParam(value = "sortReverse", required = false) String sortReverse) {
		return (List<Commande>) commandeService.getAllPageable(page, offset, numeroCommande, client, dateLivraison, sortType, sortReverse);
	}
	
	@RequestMapping(path = "/commande/count", method=RequestMethod.GET)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody long count() {
		return commandeService.count();
	}
	
	@RequestMapping(path = "/commande/currentMonth/{dateCurrentMonth}", method=RequestMethod.GET)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody List<Commande> getByDateCurrentMonth(@PathVariable Date dateCurrentMonth) {
		return (List<Commande>) commandeService.getByDateCurrentMonth(dateCurrentMonth);
	}
	
	@RequestMapping(path = "/commande/{id}", method=RequestMethod.GET)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody Commande get(@PathVariable Long id) {
		return (Commande) commandeService.get(id);
	}
	
	@RequestMapping(path = "/commande/", method=RequestMethod.PUT)
	@JsonView(View.CommandeQuantitePreparation.class)
	public @ResponseBody Commande update(@RequestBody Commande commande) {
		return (Commande) commandeService.update(commande);
	}
	
	@RequestMapping(path = "/commande", method=RequestMethod.POST)
	public @ResponseBody Commande create(@RequestBody Commande commande) {
		return (Commande) commandeService.create(commande);
	}
	
	@RequestMapping(path = "/commande/{id}", method=RequestMethod.DELETE)
	public @ResponseBody void delete(@PathVariable Long id) {
		commandeService.delete(id);
	}
	
	@RequestMapping(path = "/exporter", method=RequestMethod.GET)
	public void exporter(HttpServletResponse response,
			@RequestParam(value = "numeroCommande", required = false, defaultValue="") String numeroCommande,
			@RequestParam(value = "client", required = false, defaultValue="") String client,
			@RequestParam(value = "dateLivraison", required = false) String dateLivraison,
			@RequestParam(value = "sortType", required = false) String sortType,
			@RequestParam(value = "sortReverse", required = false) String sortReverse) {
		commandeService.exporter(response, numeroCommande, client, dateLivraison);
	}
	
	@RequestMapping(path = "/preparation/exporter", method=RequestMethod.GET)
	public void exporterPreparationParJour(HttpServletResponse response,
			@RequestParam(value = "numeroCommande", required = false, defaultValue="") String numeroCommande,
			@RequestParam(value = "client", required = false, defaultValue="") String client,
			@RequestParam(value = "dateLivraison", required = false) String dateLivraison,
			@RequestParam(value = "sortType", required = false) String sortType,
			@RequestParam(value = "sortReverse", required = false) String sortReverse) {
		commandeService.countPreparationCommande(response, numeroCommande, client, dateLivraison);
	}
	
}
